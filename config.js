const path = require("path");

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, "public/uploads"),
    db: {
        name: "controlwork_12",
        url: "mongodb://localhost"
    },
    fb: {
        appId: "2863810353857141",
        appSecret:"b11d7bce7ffed9bce3eece28182d6349"
    }
};