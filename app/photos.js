const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Photo = require('../models/Photo');
const auth = require('../middleware/auth');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


    router.get("/", async (req, res) => {
        const criteria ={};
        if (req.query.user) {
            criteria.user = req.user
        }
            try {
                const photos = await Photo.find(criteria).populate("user");
                res.send(photos);
            } catch (e) {
                res.sendStatus(500);
            }
    });
// router.get("/:id", async (req, res) => {
//     const criteria ={};
//     if (req.query.user) {
//         criteria.user = req.user
//
//     }
//     try {
//         const photo = await Photo.find(criteria).populate("user");
//
//         // if (photo) {
//             return res.send(photo);
//         // } else {
//         //     return res.status(404).send({message: 'Photo not found!'});
//         // }
//
//     } catch {
//         return res.status(404).send({message: 'Photo not found!'});
//     }
// });
    router.post("/",auth, upload.single("image"), async (req, res) => {
        const photoData = new Photo({
            title: req.body.title,
            image: req.body.image,
            user: req.user._id
        });

        if (req.file) {
            photoData.image = req.file.filename;
        }
        try {
            await photoData.save();
            res.send(photoData);
        } catch (e) {
            res.sendStatus(400);
        }
    });


    router.delete("/:id",auth, async (req, res) => {
        const item = await Photo.findById(req.params.id);

        try {
            await item.delete();
            res.send("This photo was successfully deleted");
        } catch(e) {
            console.log(e);
            res.status(500).send(e);
        }
    });


module.exports = router;